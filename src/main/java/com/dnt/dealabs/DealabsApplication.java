package com.dnt.dealabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DealabsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DealabsApplication.class, args);
    }

}
