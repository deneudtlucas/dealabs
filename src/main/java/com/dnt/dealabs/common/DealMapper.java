package com.dnt.dealabs.common;

import com.dnt.dealabs.controller.model.DealDTO;
import com.dnt.dealabs.repository.model.DealDO;
import com.dnt.dealabs.repository.model.TemperatureDO;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Component
public class DealMapper {

    public List<DealDTO> map(List<DealDO> dealDOs) {
        List<DealDTO> dealDTOs = new ArrayList<>();
        for(DealDO dealDO : dealDOs) {
            dealDTOs.add(map(dealDO));
        }
        return dealDTOs;
    }

    @Transactional
    public DealDTO map(DealDO dealDO) {
        DealDTO dealDTO = new DealDTO();
        dealDTO.setId(dealDO.getId());
        dealDTO.setTitle(dealDO.getTitle());
        dealDTO.setShopName(dealDO.getShopName());
        dealDTO.setShopLink(dealDO.getShopLink());
        dealDTO.setPriceOld(dealDO.getPriceOld());
        dealDTO.setPriceNew(dealDO.getPriceNew());
        dealDTO.setPromoCode(dealDO.getPromoCode());
        dealDTO.setCreator(dealDO.getCreator().getUsername());
        Integer temperature = 0;
        if (dealDO.getTemperatures() != null) {
            for (TemperatureDO temperatureDO : dealDO.getTemperatures()) {
                temperature += temperatureDO.getValue();
            }
        }
        dealDTO.setTemperature(temperature);
        dealDTO.setDate(dealDO.getDate());
        dealDTO.setImgUrl(dealDO.getImgUrl());
        dealDTO.setDescription(dealDO.getDescription());
        return dealDTO;
    }

    public DealDO map(DealDTO dealDTO) {
        DealDO dealDO = new DealDO();
        dealDO.setId(dealDTO.getId());
        dealDO.setTitle(dealDTO.getTitle());
        dealDO.setShopName(dealDTO.getShopName());
        dealDO.setShopLink(dealDTO.getShopLink());
        dealDO.setPriceOld(dealDTO.getPriceOld());
        dealDO.setPriceNew(dealDTO.getPriceNew());
        dealDO.setPromoCode(dealDTO.getPromoCode());
        dealDO.setDate(dealDTO.getDate());
        dealDO.setImgUrl(dealDTO.getImgUrl());
        dealDO.setDescription(dealDTO.getDescription());
        return dealDO;
    }
}
