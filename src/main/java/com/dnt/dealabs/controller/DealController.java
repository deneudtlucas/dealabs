package com.dnt.dealabs.controller;

import com.dnt.dealabs.controller.model.DealDTO;
import com.dnt.dealabs.service.DealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DealController {

    private DealService dealService;

    @Autowired
    public DealController(DealService dealService) {
        this.dealService = dealService;
    }

    @GetMapping("/public/deal")
    public List<DealDTO> findAll() {
        return dealService.findAll();
    }

    @GetMapping("/public/deal/{id}")
    public DealDTO findOne(@PathVariable Long id) {
        return dealService.findOne(id);
    }

    @PostMapping("/deal")
    public DealDTO create(@RequestBody DealDTO dealDTO) {
        return dealService.create(dealDTO);
    }

    @GetMapping("/public/testuser")
    public void createUserForTests() {
        dealService.createUserForTests();
    }

    @GetMapping("/public/testtemperature")
    public void addTemperatureForTests() {
        dealService.addTemperatureForTests();
    }

    @GetMapping("/public/testcreatedeal")
    public void createDealForTests() {
        dealService.createDealForTests();
    }
}
