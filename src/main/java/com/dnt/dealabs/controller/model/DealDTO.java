package com.dnt.dealabs.controller.model;

import lombok.Getter;
import lombok.Setter;

import java.util.GregorianCalendar;

public class DealDTO {
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String title;

    @Getter
    @Setter
    private String shopName;

    @Getter
    @Setter
    private String shopLink;

    @Getter
    @Setter
    private float priceOld;

    @Getter
    @Setter
    private float priceNew;

    @Getter
    @Setter
    private String promoCode;

    @Getter
    @Setter
    private Integer temperature;

    @Getter
    @Setter
    private String creator;

    @Getter
    @Setter
    private GregorianCalendar date;

    @Getter
    @Setter
    private String imgUrl;

    @Getter
    @Setter
    private String description;

    @Setter
    @Getter
    private float percentage;

    public DealDTO() {
    }
}
