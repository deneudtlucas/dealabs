package com.dnt.dealabs.controller.model;

import lombok.Getter;
import lombok.Setter;

public class LoginRequestDTO {
    @Getter
    @Setter
    private String identifiant;

    @Getter
    @Setter
    private String motDePasse;
}
