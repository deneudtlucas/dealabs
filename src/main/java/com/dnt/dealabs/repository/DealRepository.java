package com.dnt.dealabs.repository;

import com.dnt.dealabs.repository.model.DealDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface DealRepository extends JpaRepository<DealDO, Long> {
}
