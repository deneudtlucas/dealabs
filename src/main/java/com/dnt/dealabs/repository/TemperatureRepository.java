package com.dnt.dealabs.repository;

import com.dnt.dealabs.repository.model.DealDO;
import com.dnt.dealabs.repository.model.TemperatureDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.MANDATORY)
public interface TemperatureRepository extends JpaRepository<TemperatureDO, Long> {
}
