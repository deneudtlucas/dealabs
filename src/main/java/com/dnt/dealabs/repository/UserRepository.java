package com.dnt.dealabs.repository;

import com.dnt.dealabs.repository.model.TemperatureDO;
import com.dnt.dealabs.repository.model.UserDO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Transactional(propagation = Propagation.MANDATORY)
public interface UserRepository extends JpaRepository<UserDO, Long> {
    Optional<UserDO> findByUsername(final String username);
}
