package com.dnt.dealabs.repository.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.GregorianCalendar;
import java.util.List;

@Entity
@Table(name = "tbl_deal")
public class DealDO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private Long id;

    @Column(name = "title")
    @Getter
    @Setter
    private String title;

    @Column(name = "shop_name")
    @Getter
    @Setter
    private String shopName;

    @Column(name = "shop_link")
    @Getter
    @Setter
    private String shopLink;

    @Column(name = "price_old")
    @Getter
    @Setter
    private float priceOld;

    @Column(name = "price_new")
    @Getter
    @Setter
    private float priceNew;

    @Column(name = "promo_code")
    @Getter
    @Setter
    private String promoCode;

    @Column(name = "date")
    @Getter
    @Setter
    private GregorianCalendar date;

    @Column(name = "img_url")
    @Getter
    @Setter
    private String imgUrl;

    @Column(name = "description")
    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    @ManyToOne
    private UserDO creator;

    @Getter
    @Setter
    @OneToMany
    private List<TemperatureDO> temperatures;
}
