package com.dnt.dealabs.repository.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "tbl_temperature")
public class TemperatureDO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private Long id;

    @Column(name = "value")
    @Getter
    @Setter
    private Integer value;

    @Getter
    @Setter
    @ManyToOne
    private UserDO user;

    @Getter
    @Setter
    @ManyToOne
    private DealDO deal;
}
