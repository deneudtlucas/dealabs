package com.dnt.dealabs.repository.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.userdetails.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "tbl_user")
public class UserDO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @Getter
    @Setter
    private Long id;

    @Column(name = "username")
    @Getter
    @Setter
    private String username;

    @Column(name = "first_name")
    @Getter
    @Setter
    private String firstName;

    @Column(name = "last_name")
    @Getter
    @Setter
    private String lastName;

    @Column(name = "password")
    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    @OneToMany
    private List<TemperatureDO> temperatures;

    @Getter
    @Setter
    @OneToMany
    private List<DealDO> deals;
}
