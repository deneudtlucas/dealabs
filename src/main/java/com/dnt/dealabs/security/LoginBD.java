package com.dnt.dealabs.security;

import javax.servlet.http.HttpServletRequest;

import com.dnt.dealabs.controller.model.LoginRequestDTO;
import com.dnt.dealabs.controller.model.UtilisateurDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * @author MDE
 *
 */
@RestController
@RequestMapping(value = "/public/login")
@Transactional
public class LoginBD {

	private static final Logger logger = LoggerFactory.getLogger(LoginBD.class);

	@Autowired
	private AuthenticationProvider authenticationManager;


	/**
	 * methode de connexion d'un utilisateur
	 *
	 * @param request données necessaire a la connexion
	 * @return
	 * @throws RestException
	 * @throws Exception
	 */
	@PostMapping
	public UtilisateurDTO login(@RequestBody final LoginRequestDTO request, final HttpServletRequest req) {

		// Controle des params obligatoires
		if (StringUtils.isEmpty(request.getIdentifiant()) || StringUtils.isEmpty(request.getMotDePasse())) {
			throw new BadCredentialsException("User/pwd must not be emtpy");
		}

		final Authentication authentication = authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getIdentifiant(), request.getMotDePasse()));
		if (authentication == null) {
			throw new BadCredentialsException("User/pwd incorrect");
		}

		final DlabsSpringUser utilisateur = (DlabsSpringUser) authentication.getPrincipal();
		logger.debug("New user logged : " + utilisateur.getUsername());

		return new UtilisateurDTO(request.getIdentifiant(), request.getMotDePasse(), new ArrayList<>());
	}

}
