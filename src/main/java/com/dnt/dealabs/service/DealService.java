package com.dnt.dealabs.service;

import com.dnt.dealabs.common.DealMapper;
import com.dnt.dealabs.controller.model.DealDTO;
import com.dnt.dealabs.repository.DealRepository;
import com.dnt.dealabs.repository.TemperatureRepository;
import com.dnt.dealabs.repository.UserRepository;
import com.dnt.dealabs.repository.model.DealDO;
import com.dnt.dealabs.repository.model.TemperatureDO;
import com.dnt.dealabs.repository.model.UserDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class DealService {

    private DealRepository dealRepository;
    private UserRepository userRepository;
    private TemperatureRepository temperatureRepository;

    private DealMapper dealMapper;

    @Autowired
    public DealService(DealRepository dealRepository, DealMapper dealMapper, UserRepository userRepository, TemperatureRepository temperatureRepository) {
        this.dealRepository = dealRepository;
        this.userRepository = userRepository;
        this.dealMapper = dealMapper;
        this.temperatureRepository = temperatureRepository;
    }

    @Transactional
    public List<DealDTO> findAll() {
        List<DealDO> dealDOs = dealRepository.findAll();
        List<DealDTO> dealDTOs = dealMapper.map(dealDOs);
        return dealDTOs;
    }

    @Transactional
    public DealDTO findOne(Long id) {
        DealDO dealDO = dealRepository.findById(id).get();
        DealDTO dealDTO = dealMapper.map(dealDO);
        float percentage = Math.round(dealDTO.getPriceNew() / dealDTO.getPriceOld() * 100);
        dealDTO.setPercentage(percentage);
        return dealDTO;
    }

    @Transactional
    public DealDTO create(DealDTO dealDTO) {
        DealDO dealDO = dealMapper.map(dealDTO);
        UserDO userDO = userRepository.findById(1l).get();
        dealDO.setCreator(userDO);
        dealDO = dealRepository.save(dealDO);

        return dealMapper.map(dealDO);
    }

    @Transactional
    public void createUserForTests() {
        UserDO userDO = new UserDO();
        userDO.setUsername("NyneTeaNyne");
        userDO.setFirstName("Lucas");
        userDO.setLastName("Deneudt");
        userDO.setPassword("Password");
        userRepository.save(userDO);
    }

    @Transactional
    public void addTemperatureForTests() {
        UserDO userDO = userRepository.findById(1l).get();
        DealDO dealDO = dealRepository.findById(2l).get();

        TemperatureDO temperatureDO = new TemperatureDO();
        temperatureDO.setDeal(dealDO);
        temperatureDO.setUser(userDO);
        temperatureDO.setValue(1);

        List<TemperatureDO> temperatures = new ArrayList<>();
        temperatures.add(temperatureDO);
        temperatureRepository.save(temperatureDO);

        dealDO.setTemperatures(temperatures);
        dealRepository.save(dealDO);
        userDO.setTemperatures(temperatures);
        userRepository.save(userDO);
    }

    public void createDealForTests() {
        UserDO userDO = userRepository.findById(1l).get();

        DealDO dealDO = new DealDO();
        dealDO.setTitle("Titre");
        dealDO.setShopName("Dealabs");
        dealDO.setShopLink("https://www.dealabs.com/");
        dealDO.setPriceOld(50);
        dealDO.setPriceNew(30);
        dealDO.setPromoCode("PROMO");
        dealDO.setDate(new GregorianCalendar());
        dealDO.setImgUrl("https://picsum.photos/200");
        dealDO.setDescription("Description.");
        dealDO.setCreator(userDO);

        dealRepository.save(dealDO);
    }
}
