new Vue({
    el: '#app',
    data: {
        deal: null
    },
    mounted() {
        axios
            .get('http://localhost:8080/public/deal/' + new URLSearchParams(window.location.search).get('id'))
            .then(response => (this.deal = response.data))
    }
})