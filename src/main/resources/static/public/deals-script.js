new Vue({
    el: '#app',
    data: {
        deals: []
    },
    mounted() {
        axios
            .get('http://localhost:8080/public/deal')
            .then(response => (this.deals = response.data))
    }
})