new Vue({
    el: '#app',
    data: {
        username: "",
        password: ""
    },
    methods: {
        checkForm(e) {
            axios.post('http://localhost:8080/public/login', {
                identifiant: this.username,
                motDePasse: this.password
            }).then(function (response) {
                localStorage.setItem('auth', btoa(response.data.username + ":" + response.data.password))
                window.location.href = "http://localhost:8080/public/deals.html"
            })
            e.preventDefault()
        }
    }
})