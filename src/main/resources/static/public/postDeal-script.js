new Vue({
    el: '#app',
    data: {
        title: "",
        description: "",
        priceOld: "",
        priceNew: "",
        shopName: "",
        promoCode: "",
        imgUrl: "",
        shopLink: ""
    },
    methods: {
        checkForm: function (e) {
            axios.post('http://localhost:8080/deal', {
                title: this.title,
                description: this.description,
                priceOld: this.priceOld,
                priceNew: this.priceNew,
                shopName: this.shopName,
                promoCode: this.promoCode,
                imgUrl: this.imgUrl,
                shopLink: this.shopLink
            }, {
                headers: {
                    'Authorization': `Basic ` + localStorage.getItem('auth')
                }
            }).then(function (response) {
                window.location.href = "http://localhost:8080/public/deals.html"
            })
            e.preventDefault()
        }
    }
})